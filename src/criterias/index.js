/**
 * Created by ahmadnasrawi on 8/20/17.
 */
import React from 'react';
// import _ from 'lodash';

// import DateTimeInput from '../components/DateTimePicker';
import {
    Datagrid,
    Edit,
    Create,
    FormTab,
    List,
    Filter,
    TabbedForm,
    TextInput,
    TextField,
    ReferenceInput,
    ImageInput,
    ImageField,
    RadioButtonGroupInput,
    SelectArrayInput,
    LongTextInput,
    AutocompleteInput,
    translate,
    // Delete,
    NumberInput,
    ReferenceField,
    FormDataConsumer,
    SelectInput,
    EditButton,
    DeleteButton
} from 'react-admin';

// import Icon from '@material-ui/icons/Person';


// export const CampaignIcon = Icon;

// const CampaignFilter = props => (
//     <Filter {...props}>
//         <TextInput label="pos.search" source="q" alwaysOn />
//         <TextInput label="Name" source="name" />
//         <TextInput label="Description" source="description" />
//         <TextInput label="Notes" source="notes" />
//     </Filter>
// );

export const CriteriasList = props => (
    <List {...props} perPage={20}>
        <Datagrid>
            <TextField
                source="id"
                headerStyle={{
                    textAlign: 'center',
                    width: 300,
                }}
            />
            <ReferenceField
                source="pool_id"
                label="Pool"
                reference="pools"
            >
                <TextField source="name" />
            </ReferenceField>
            <TextField
                source="topic"
                // label="resources.campaigns.fields.name_arabic"
                headerStyle={{
                    textAlign: 'center',
                    width: 300,
                }}
            />
            <TextField
                source="question"
                // label="resources.campaigns.fields.name_arabic"
                headerStyle={{
                    textAlign: 'center',
                    width: 300,
                }}
            />
            <TextField
                source="type"
                // label="resources.campaigns.fields.description"
                headerStyle={{
                    textAlign: 'center',
                    width: 300,
                }}
            />
            <EditButton />
            <DeleteButton />
        </Datagrid>
    </List>
);

const featurableTypes = [
    { id: 'src', name: 'Upload Image' },
    { id: 'url', name: 'Url Image' },
];

export const CriteriasCreate = props => (
    <Create {...props}>
        <TabbedForm validate={validateCampaignCreation} submitOnEnter={false}>
            <FormTab
                label="Create Criteria"
            >

                {/*'topic'     => 'required|string|max:191',*/}
                {/*'question'  => 'required|string|max:191',*/}
                {/*'pool_id'   => 'required|integer|exists:pools,id',*/}
                {/*'type'      => 'nullable|integer|digits_between:1,11',*/}
                <TextInput
                    source="question"
                    validation={{
                        required: true,
                    }}
                />
                <TextInput
                    source="topic"
                    validation={{
                        required: true,
                    }}
                />
                <SelectInput
                    source="type"
                    choices={[
                        { id: 1, name: 'Yes/No' },
                        { id: 2, name: '1 - 10' },
                    ]}
                    validation={{
                        required: true,
                    }}
                />
                <ReferenceInput
                    source="pool_id"
                    reference="pools"
                    validation={{
                        required: true,
                    }}
                >
                    <AutocompleteInput source="name"/>
                </ReferenceInput>
                {/*<TextInput*/}
                    {/*source="name_arabic"*/}
                    {/*style={{*/}
                        {/*marginLeft: 32,*/}
                    {/*}}*/}
                    {/*validation={{*/}
                        {/*required: true,*/}
                    {/*}}*/}
                {/*/>*/}
                {/*<LongTextInput*/}
                    {/*source="description"*/}
                    {/*style={{*/}
                        {/*marginLeft: 32,*/}
                    {/*}}*/}
                    {/*validation={{*/}
                        {/*required: true,*/}
                    {/*}}*/}
                {/*/>*/}
                {/*<LongTextInput*/}
                    {/*source="description_arabic"*/}
                    {/*style={{*/}
                        {/*marginLeft: 32,*/}
                    {/*}}*/}
                    {/*validation={{*/}
                        {/*required: true,*/}
                    {/*}}*/}
                {/*/>*/}
                {/*<LongTextInput*/}
                    {/*source="notes"*/}
                    {/*style={{*/}
                        {/*marginLeft: 32,*/}
                    {/*}}*/}
                    {/*validation={{*/}
                        {/*required: true,*/}
                    {/*}}*/}
                {/*/>*/}
                {/*<NumberInput*/}
                    {/*source="budget"*/}
                    {/*style={{*/}
                        {/*marginLeft: 32,*/}
                    {/*}}*/}
                    {/*validation={{*/}
                        {/*required: true,*/}
                    {/*}}*/}
                {/*/>*/}
                {/*<NumberInput*/}
                    {/*source="discount_percentage"*/}
                    {/*style={{*/}
                        {/*marginLeft: 32,*/}
                    {/*}}*/}
                    {/*validation={{*/}
                        {/*required: true,*/}
                    {/*}}*/}
                {/*/>*/}
                {/*<ReferenceInput*/}
                    {/*label="resources.campaigns.fields.tone"*/}
                    {/*style={{*/}
                        {/*marginLeft: 32,*/}
                    {/*}}*/}
                    {/*source="tones"*/}
                    {/*reference="tones"*/}
                    {/*allowEmpty*/}
                {/*>*/}
                    {/*<SelectArrayInput optionText="name" />*/}
                {/*</ReferenceInput>*/}
            </FormTab>
        </TabbedForm>
    </Create>
);

// const CampaignTitle = ({ record }) => record ? <span>Edit {record.name}</span> : null;
//
export const CriteriaEdit = props => (
    <Edit {...props}>
        <TabbedForm validate={validateCampaignCreation} submitOnEnter={false}>
            <FormTab label="Edit Criteria">
                <TextInput
                    source="question"
                    validation={{
                        required: true,
                    }}
                />
                <TextInput
                    source="topic"
                    validation={{
                        required: true,
                    }}
                />
                <SelectInput
                    source="type"
                    choices={[
                        { id: 1, name: 'Yes/No' },
                        { id: 2, name: '1 - 10' },
                    ]}
                    validation={{
                        required: true,
                    }}
                />
                <ReferenceInput
                    source="pool_id"
                    reference="pools"
                    validation={{
                        required: true,
                    }}
                >
                    <AutocompleteInput source="name"/>
                </ReferenceInput>
            </FormTab>
        </TabbedForm>
    </Edit>
);

// const CampaignsDeleteTitle = translate(({ record, translate }) => (<span>
//     {translate('resources.campaigns.actions.delete')}&nbsp;
//     {record && `${record.name}`}
// </span>));
//
// export const CampaignsDelete = props => <Delete {...props} title={<CampaignsDeleteTitle />} />;


const validateCampaignCreation = (elements) => {
    const errors = {};
    if (!elements.name) {
        errors.name = ['Campaign Name is required'];
    }
    if (!elements.name_arabic) {
        errors.name_arabic = ['Campaign Arabic Name is required'];
    }
    if (!elements.description) {
        errors.description = ['Campaign description is required'];
    }
    if (!elements.description_arabic) {
        errors.description_arabic = ['Campaign Arabic description is required'];
    }
    if (!elements.budget) {
        errors.budget = ['Campaign budget is required'];
    }
    if (!elements.discount_percentage) {
        errors.discount_percentage = ['Campaign discount percentage is required'];
    }
    if (elements.tones && !elements.tones.data || !elements.tones) {
        if (elements.tones && !elements.tones.data) {
            errors.tones.data = ['Campaign tones is required'];
        } else if (!elements.tones) {
            errors.tones = ['Campaign tones is required'];
        }
    }
    if (!elements.start_date) {
        errors.start_date = ['Campaign start date is required'];
    }
    if (!elements.end_date) {
        errors.end_date = ['Campaign end date is required'];
    }
    return errors;
};
