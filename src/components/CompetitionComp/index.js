import React, {
    Component
} from 'react';

import {
    Card,
    CardContent,
    CardHeader,
    Grid,
    Typography,
    IconButton,
    Collapse,
    Button,
    Modal,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogContentText,
    DialogActions
} from '@material-ui/core';
import { withRouter } from "react-router";
import axios from 'axios';
import PropTypes from 'prop-types';
import CircularProgress from "@material-ui/core/CircularProgress";
import {
    ENDPOINT
} from '../../config';

const token = localStorage.getItem('token');

class CompetitionCom extends Component{
    state = {
        checked: false,
        confirm: false,
        disable: false
    }

    delete = () => {
        this.setState({disable: true});
        axios.delete(`${ENDPOINT}/competitions/${this.props.competition.id}`,{
            headers: {
                Accept: 'application/json',
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => {
                this.setState({
                    confirm: false,
                    disable: false
                });
                this.props.reload()
            }).catch(error => {
            console.log(error)
        })
    };

    render(){
        const {
            competition
        } = this.props;
        return (
            <Card
                style={{
                    margin: '15px 0'
                }}>
                <CardContent
                    style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        alignItems: 'center'
                    }}
                >
                    <Grid
                        style={{
                            display: 'flex',
                            alignItems: 'center',
                            width: '75%'
                        }}
                        onClick={() =>
                            this.props.history.push({
                                pathname: '/competition-details',
                                state: {competitionData: competition}
                            })
                        }
                    >
                        <Grid>
                            <img src={competition.logo ? competition.logo : require('../../judging.png')} width="100" />
                        </Grid>
                        <Grid
                            style={{
                                marginLeft: 10,
                                width: '60%'
                            }}
                        >
                        <span
                            style={{
                                fontSize: 25,
                                display: 'block'
                            }}
                        >{competition.name}</span>
                            <span
                                style={{
                                    fontSize: 20
                                }}
                            >{competition.description}</span>
                        </Grid>
                    </Grid>
                    <Grid
                        style={{
                            width: '25%'
                        }}
                    >
                        <Button>
                            Edit
                        </Button>
                        <Button
                            onClick={() => this.setState({confirm: true})}
                        >
                            Delete
                        </Button>
                    </Grid>
                </CardContent>
                <Dialog
                    fullScreen={false}
                    open={this.state.confirm}
                    onClose={() => this.setState({confirm: false})}
                    aria-labelledby="responsive-dialog-title"
                >
                    <DialogContent>
                        <Grid
                            style={{
                                padding: 20
                            }}
                        >
                            <span>Are You sure you want to delete this competition</span>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.delete} color="primary">
                            {this.state.disable ? <CircularProgress size={20} /> : ""}
                            &nbsp; yes
                        </Button>
                        <Button onClick={() => this.setState({confirm: false})} color="primary" autoFocus>
                            No
                        </Button>
                    </DialogActions>
                </Dialog>
            </Card>
        );
    }
}
CompetitionCom.propTypes = {
    history: PropTypes.object.isRequired
};

export default withRouter(CompetitionCom);