/**
 * Created by ahmadnasrawi on 8/20/17.
 */
import React from 'react';
// import _ from 'lodash';

// import DateTimeInput from '../components/DateTimePicker';
import {
    Datagrid,
    Edit,
    Create,
    FormTab,
    List,
    Filter,
    TabbedForm,
    TextInput,
    TextField,
    ReferenceInput,
    ImageInput,
    ImageField,
    RadioButtonGroupInput,
    SelectArrayInput,
    LongTextInput,
    AutocompleteInput,
    translate,
    // Delete,
    NumberInput,
    EditButton,
    DeleteButton
} from 'react-admin';

// import Icon from '@material-ui/icons/Person';


// export const CampaignIcon = Icon;

// const CampaignFilter = props => (
//     <Filter {...props}>
//         <TextInput label="pos.search" source="q" alwaysOn />
//         <TextInput label="Name" source="name" />
//         <TextInput label="Description" source="description" />
//         <TextInput label="Notes" source="notes" />
//     </Filter>
// );

export const PoolsList = props => (
    <List {...props} perPage={20}>
        <Datagrid>
            <TextField
                source="name"
                headerStyle={{
                    textAlign: 'center',
                    width: 300,
                }}
            />
            <TextField
                source="score_unit"
                // label="resources.campaigns.fields.description"
                headerStyle={{
                    textAlign: 'center',
                    width: 300,
                }}
            />
            <EditButton />
            <DeleteButton />
        </Datagrid>
    </List>
);

const featurableTypes = [
    { id: 'src', name: 'Upload Image' },
    { id: 'url', name: 'Url Image' },
];

export const PoolCreate = props => (
    <Create title="Create Pool" {...props}>
        <TabbedForm validate={validateCampaignCreation} submitOnEnter={false}>
            <FormTab
                label="resources.campaigns.actions.tab_info"
            >
                <TextInput
                    source="name"
                    style={{
                        marginLeft: 32,
                    }}
                    validation={{
                        required: true,
                    }}
                />
                <NumberInput
                    source="score_unit"
                    style={{
                        marginLeft: 32,
                    }}
                    validation={{
                        required: true,
                    }}
                />
            </FormTab>
        </TabbedForm>
    </Create>
);
//
// const CampaignTitle = ({ record }) => record ? <span>Edit {record.name}</span> : null;
//
export const PoolEdit = props => (
    <Edit {...props}>
        <TabbedForm validate={validateCampaignCreation} submitOnEnter={false}>
            <FormTab label="Pool Edit">
                <TextInput
                    source="name"
                    style={{
                        marginLeft: 32,
                    }}
                    validation={{
                        required: true,
                    }}
                />
                <NumberInput
                    source="score_unit"
                    style={{
                        marginLeft: 32,
                    }}
                    validation={{
                        required: true,
                    }}
                />
            </FormTab>
        </TabbedForm>
    </Edit>
);

// const CampaignsDeleteTitle = translate(({ record, translate }) => (<span>
//     {translate('resources.campaigns.actions.delete')}&nbsp;
//     {record && `${record.name}`}
// </span>));
//
// export const CampaignsDelete = props => <Delete {...props} title={<CampaignsDeleteTitle />} />;


const validateCampaignCreation = (elements) => {
    const errors = {};
    if (!elements.name) {
        errors.name = ['Campaign Name is required'];
    }
    if (!elements.name_arabic) {
        errors.name_arabic = ['Campaign Arabic Name is required'];
    }
    if (!elements.description) {
        errors.description = ['Campaign description is required'];
    }
    if (!elements.description_arabic) {
        errors.description_arabic = ['Campaign Arabic description is required'];
    }
    if (!elements.budget) {
        errors.budget = ['Campaign budget is required'];
    }
    if (!elements.discount_percentage) {
        errors.discount_percentage = ['Campaign discount percentage is required'];
    }
    if (elements.tones && !elements.tones.data || !elements.tones) {
        if (elements.tones && !elements.tones.data) {
            errors.tones.data = ['Campaign tones is required'];
        } else if (!elements.tones) {
            errors.tones = ['Campaign tones is required'];
        }
    }
    if (!elements.start_date) {
        errors.start_date = ['Campaign start date is required'];
    }
    if (!elements.end_date) {
        errors.end_date = ['Campaign end date is required'];
    }
    return errors;
};
