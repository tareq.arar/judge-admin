 import HttpError from './HttpError';
 // import * as toasts from './toast.js';

 export const fetchJson = (url, options = {}) => {
     const requestHeaders = options.headers || new Headers({
         Accept: 'application/json',
     });
     if (!(options && options.body && options.body instanceof FormData)) {
         requestHeaders.set('Content-Type', 'application/json');
     }
     if (options.user && options.user.authenticated && options.user.token) {
         requestHeaders.set('Authorization', options.user.token);
     }

     return fetch(url, { ...options, headers: requestHeaders }).then(response => response.text().then((text) => {
         let Databody = null;
         let total = 0;
         try {
             var EntireResponse = JSON.parse(text);
             if (JSON.stringify(JSON.parse(text).data) === undefined) {
                 const signleobj = JSON.parse(text);

                 Databody = JSON.stringify(signleobj);
             //  Databody.push({author_id:5});
               // console.info(Databody);
             } else {
                 Databody = JSON.stringify(JSON.parse(text).data);
                 total = JSON.stringify(JSON.parse(text).meta.total);
             }
         } catch (ex) {}
         return {
             status: response.status,
             statusText: response.statusText,
             headers: response.headers,
             body: Databody,
             total,
             entrireresponse: EntireResponse,
         };
     })).then((_ref) => {
         let status = _ref.status,
             statusText = _ref.statusText,
             headers = _ref.headers,
             body = _ref.body,
             total = _ref.total,
             EntireResponse = _ref.entrireresponse;
         let json = void 0;
         try {
             json = JSON.parse(body);
         } catch (e) {
            // not json, no big deal
         }
         if (status < 200 || status >= 300) {
             // try {
             //     const arr = Object.keys(EntireResponse).map(key => EntireResponse[key]);
             //     for (let i = 0; i < arr.length; i++)
             //         toasts.Toast(arr[i][0], null, null);
             // } catch (e) {
             //    // not json, no big deal
             // }
             return Promise.reject(new HttpError((json && json.message) || statusText, status));
         }
         return { status, headers, body, json, total };
     });
 };

 export const queryParameters = data => Object.keys(data)
    .map(key => [key, data[key]].map(encodeURIComponent).join('='))
    .join('&');
