import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';

import {
    connect
} from 'react-redux';

import {
    Grid,
    MenuItem
} from '@material-ui/core';
import {
    reduxForm,
    Field,
    FieldArray,
    Form,
    submit
} from 'redux-form';

import {
    renderTextField,
    renderMultiLineField
} from '../../FormFields';
import {ENDPOINT} from "../../config";
import axios from "axios/index";

const validate = values => {
    const errors = {};
    const requiredFields = ["name", "description"];
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = "Required";
        }
    });
    return errors;
};



class NewPanelForm extends React.Component {

    state = {
        poolId: null
    };
    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.props.handleClose()
    };

    submit = (data) => {
        const token = localStorage.getItem('token');
        if(!this.props.isEdit) {
            if(!this.state.poolId){
                this.createPool(data.name, (success) => {
                    axios.post(`${ENDPOINT}/competitions`, {
                        name: data.name,
                        description: data.description,
                        date: data.date ? data.date : null,
                        pool_id: this.state.poolId
                    }, {
                        headers: {
                            Accept: 'application/json',
                            Authorization: `Bearer ${token}`
                        }
                    }).then((response) => {
                        // let compDetails = response.data.data;
                        // this.props.setCompetitionId(compDetails.id)
                        // this.props.handleNext()
                        this.handleClose()
                    }).catch(error => {
                        // this.setState({
                        // })
                    });
                });
            }
        } else {
            axios.patch(`${ENDPOINT}/competitions/${this.props.currentCompId}?name=${data.name}&description=${data.description}&pool_id=${data.pool_id}${data.date ? `&date=${data.date}` : ''}`,null,{
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${token}`
                }
            }).then((response) => {
                let compDetails = response.data.data;
                this.props.setCompetitionId(compDetails.id)
                this.props.handleNext()
            }).catch(error => {
                console.log(error)
            });

        }
    };

    render() {
        const { handleSubmit } = this.props;
        return (
            <div>
                <Dialog
                    open={this.props.newPanel}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <Grid
                        style={{
                            padding: 29,
                            fontSize: 27
                        }}
                    >
                        <span id="form-dialog-title">New Panel</span>
                    </Grid>
                    <DialogContent>
                        <Grid
                            style={{
                                minWidth: 600
                            }}
                        >
                            Please fill in panel details.
                        </Grid>
                        <Form
                            onSubmit={handleSubmit(this.submit)}
                        >
                            <Grid style={{display: 'flex'}}>
                                <Grid style={{width: '100%'}}>
                                    <Field
                                        id="name"
                                        name={`name`}
                                        label="Name"
                                        component={renderTextField}
                                        fullWidth
                                    />
                                </Grid>
                                <Grid style={{width: '100%'}}>
                                    <Field
                                        id="room"
                                        name={`room`}
                                        label="Room"
                                        component={renderTextField}
                                        fullWidth
                                    />
                                </Grid>
                            </Grid>

                        </Form>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.props.handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={() => this.props.dispatch(submit('NewPanel'))} color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default connect()(reduxForm({
    form: 'NewPanelForm',
    validate,
    destroyOnUnmount: true
})(NewPanelForm))