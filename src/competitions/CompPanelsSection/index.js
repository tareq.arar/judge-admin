import React, { Component } from 'react';

import {
    Grid,
    Button
} from '@material-ui/core';

import NewPanelForm from './NewPanelForm';
import PanelCard from './PanelCard';
import {
    ENDPOINT
} from '../../config';
import axios from 'axios';
import _ from 'lodash';

import JudgesSection from '../JudgesSection/index';

class CompPanelsSection extends Component{

    state = {
        CompetitionPanels: {},
        newPanel: false,
        assignJudge: false,
        assignExisting: false
    };

    getData = () => {
        const token = localStorage.getItem('token');
        axios.get(`${ENDPOINT}/competitions/${this.props.competition.id}/panels`,
            {
                headers:{
                    Accept: 'application/json',
                    Authorization: `Bearer ${token}`
                }
            })
            .then(response => {
                let panels = response.data.data.panels;
                this.props.panelId(panels[0].id)
                if(panels.length > 1){
                    this.setState({
                        CompetitionPanels: panels
                    })
                } else {
                    this.setState({
                        CompetitionPanels: panels
                    })
                }
            })
            .catch(error => {
            });
    };

    componentDidMount(){
        this.getData();
    }

    render(){
        return (
            <Grid
                style={{
                    marginTop: 30
                }}
            >
                <Grid>
                    <Grid
                        style={{
                            display: 'flex',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            width: '95%',
                            margin: '0 auto'
                        }}
                    >
                        <Grid>
                        <span
                            style={{
                                fontSize: 30
                            }}
                        >
                            Panels
                        </span>
                        </Grid>
                    </Grid>
                    <Grid
                        style={{
                            width: '100%',
                            display: 'flex',
                            flexWrap: 'wrap',
                            boxSizing: 'border-box',
                            justifyContent: this.state.CompetitionPanels.length > 1 ? 'center' : null
                        }}
                    >
                        {
                            !_.isEmpty(this.state.CompetitionPanels) ?
                                _.map(this.state.CompetitionPanels, (CompetitionPanel, key) => {
                                    return <PanelCard
                                        key={key}
                                        panelData={CompetitionPanel}
                                        assignExisting={() => this.setState({assignExisting: true})}
                                    >
                                        <JudgesSection
                                            panelId={CompetitionPanel.id}
                                            compId={this.props.competition.id}
                                            panelData={CompetitionPanel}
                                            assignExisting={this.state.assignExisting}
                                            handleClose={() => this.setState({assignExisting: false})}
                                        />
                                    </PanelCard>
                                })
                                :
                                <Grid>
                                    <span>There is no panels</span>
                                </Grid>
                        }
                    </Grid>
                </Grid>
                <NewPanelForm
                    assignJudge={this.state.assignJudge}
                    handleClose={() => this.setState({newPanel: false})} />
                <NewPanelForm
                    newPanel={this.state.newPanel}
                    handleClose={() => this.setState({newPanel: false})} />
            </Grid>
        );
    }
}

export default CompPanelsSection;