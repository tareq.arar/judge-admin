import React, { Component } from 'react';
import {
    Card,
    CardHeader,
    CardContent,
    Typography,
    Grid,
    Button
} from '@material-ui/core';

class PanelCard extends Component{
    render(){
        const { panelData, children } = this.props;
        return(
            <Card
                style={{
                    width: 500,
                    maxWidth: 500,
                    margin: 10
                }}
            >
                <CardContent>
                    <Grid
                        style={{
                            display: 'flex',
                            justifyContent: 'space-between',
                            alignItems: 'center'
                        }}
                    >
                        <Grid>
                            <span
                                style={{
                                    fontSize: 26,
                                    display: 'block'
                                }}
                            >
                                {panelData.name}
                            </span>
                                    <span>
                                {panelData.room}
                            </span>
                        </Grid>
                        <Grid>
                            <Button
                                onClick={() => this.props.assignExisting()}
                            >
                                Assign Existing
                            </Button>
                        </Grid>
                    </Grid>
                    {children}
                </CardContent>
            </Card>
        );
    }
}

export default PanelCard;