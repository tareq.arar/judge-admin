import React, { Component } from 'react';
import _ from 'lodash';
import {
    ENDPOINT
} from '../../config';


import {
    Grid,
    Button
} from '@material-ui/core';

import CompPanelsSection from '../CompPanelsSection';
import CompQuestionSection from '../CompQuestionsSection';
import CompCompetitorSection from '../CompCompetitorsSection';
class CompetitionDetails extends Component{

    state = {
        competitionDetails: {},
        isMultiPanels: false,
        hasPanel: false
    };

    componentDidMount(){
        let competitionDetails = {};
        let isMultiPanels = false;
        let hasPanel = false;
        if(this.props.location && this.props.location.state && this.props.location.state.competitionData){
            let data = this.props.location.state.competitionData;
            if(!_.isEmpty(data.panels) && data.panels.length > 1) {
                isMultiPanels = true;
                hasPanel = true;
            } else if(!_.isEmpty(data.panels) && data.panels.length > 0){
                isMultiPanels = false;
                hasPanel = true;
            }
            this.setState({
                competitionDetails: this.props.location.state.competitionData,
                isMultiPanels,
                hasPanel
            })
        } else {

        }
    }

    // created_at: "2018-09-28 17:31:18"
    // date: null
    // description: "Zain almobadarah 3 hosted by Zinc"
    // id: 1
    // name: "Zain Almobadarah 3"
    // pool_id: 1
    // slug: null
    // status: null
    // updated_at: "2018-09-28 17:31:18"

    render(){
        const { competitionDetails } = this.state;
        return (
            _.isEmpty(this.state.competitionDetails) ?
                null
                :
                <Grid>
                    <Grid
                        style={{
                            padding: 10
                        }}
                    >
                        <Grid
                            style={{
                                display: 'flex',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                                width: '95%',
                                margin: '0 auto'
                            }}
                        >
                            <Grid>
                                <Grid>
                                    <Grid>
                                        <span
                                            style={{
                                                fontSize: 40
                                            }}
                                        >{competitionDetails.name}</span>
                                    </Grid>
                                </Grid>
                                <span
                                    style={{
                                        fontSize: 20
                                    }}
                                >
                                    {competitionDetails.description}
                                </span>
                            </Grid>
                            <Grid>
                                <img src={competitionDetails.logo} width="100" height="100"
                                     style={{
                                         borderRadius: '50%'
                                     }}
                                />
                            </Grid>
                        </Grid>
                        <CompPanelsSection
                            competition={competitionDetails}
                            panelId={(id) => this.setState({panelId: id})}
                        />
                        <CompQuestionSection
                            panelId={this.state.panelId}
                            competition={competitionDetails}
                        />
                        <CompCompetitorSection
                            panelId={this.state.panelId}
                            competition={competitionDetails}
                        />
                    </Grid>
                </Grid>
        );
    }
}

export default CompetitionDetails;