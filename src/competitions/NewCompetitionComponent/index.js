import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';

import {
    connect
} from 'react-redux';

import {
    Grid,
    MenuItem
} from '@material-ui/core';
import {
    reduxForm,
    Field,
    FieldArray,
    Form,
    submit
} from 'redux-form';

import {
    renderTextField,
    renderMultiLineField,
    UploadFile
} from '../../FormFields';
import {ENDPOINT} from "../../config";
import axios from "axios/index";


const validate = values => {
    const errors = {};
    const requiredFields = ["name", "description"];
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = "Required";
        }
    });
    return errors;
};



class NewCompetition extends React.Component {

    state = {
        poolId: null,
    };

    handleClose = () => {
        this.setState({ open: false });
    };
    createPool = (compName, success) => {
        const token = localStorage.getItem('token');
        axios.post(`${ENDPOINT}/pools`, {
            name: compName,
            score_unit: 60
        }, {
            headers: {
                Accept: 'application/json',
                Authorization: `Bearer ${token}`
            }
        }).then((response) => {
            this.setState({poolId: response.data.data.id})
            success && success()
        }).catch(error => {
        });
    };

    submit = (data) => {
        const token = localStorage.getItem('token');
        if(!this.props.isEdit) {
            if(!this.state.poolId){
                this.createPool(data.name, (success) => {
                    let formData = new FormData();
                    formData.append("name", data.name);
                    formData.append("description", data.description);
                    formData.append("date", data.date ? data.date : null);
                    formData.append("pool_id", this.state.poolId);
                    formData.append("logo", data.logo[0]);

                    axios.post(`${ENDPOINT}/competitions`, formData, {
                        headers: {
                            Accept: 'application/json',
                            Authorization: `Bearer ${token}`,
                            'Content-Type': 'multipart/form-data'
                        }
                    }).then((response) => {
                        this.props.created(response.data.data)
                    }).catch(error => {});
                });
            }
        } else {
            axios.patch(`${ENDPOINT}/competitions/${this.props.currentCompId}?name=${data.name}&description=${data.description}&pool_id=${data.pool_id}${data.date ? `&date=${data.date}` : ''}`,null,{
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${token}`
                }
            }).then((response) => {
                let compDetails = response.data.data;
                this.props.setCompetitionId(compDetails.id)
                this.props.handleNext()
            }).catch(error => {
                console.log(error)
            });

        }
    };

    render() {
        const { handleSubmit } = this.props;
        return (
            <div>
                <Dialog
                    open={true}
                    onClose={this.props.close}
                    aria-labelledby="form-dialog-title"
                    classes={{
                        paper: {
                            root: {minWidth: 600}
                        }
                    }}
                >
                    <Grid
                        style={{
                            padding: 29,
                            fontSize: 27
                        }}
                    >
                        <span id="form-dialog-title">New Competition</span>
                    </Grid>
                    <DialogContent>
                        <Grid
                            style={{
                                minWidth: 600
                            }}
                        >
                            Please fill in main competition details.
                        </Grid>
                        <Form
                            onSubmit={handleSubmit(this.submit)}
                        >
                            <Grid>
                                <Field
                                    name="name"
                                    type="text"
                                    component={renderTextField}
                                    label="Name"
                                    fullWidth
                                />
                            </Grid>
                            <Grid>
                                <Field
                                    name="description"
                                    type="text"
                                    component={renderMultiLineField}
                                    label="Description"
                                    fullWidth
                                />
                            </Grid>
                            <Grid
                                style={{
                                    display: 'flex',
                                    justifyContent: 'space-between',
                                    alignItems: 'center'
                                }}
                            >
                                <Field
                                    type="date"
                                    name="date"
                                    component={renderTextField}
                                    label="Competition date"
                                    defaultValue="2017-05-24T10:30"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                                <Field
                                    label="Upload Logo"
                                    component={UploadFile}
                                    name="logo"
                                    accept=".png"
                                />

                            </Grid>
                        </Form>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.props.close} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={() => this.props.dispatch(submit('NewCompetitionForm'))} color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default connect()(reduxForm({
    form: 'NewCompetitionForm',             // <------ same form name
    validate,
    destroyOnUnmount: true
})(NewCompetition))