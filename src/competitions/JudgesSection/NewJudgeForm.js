import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';

import {
    connect
} from 'react-redux';

import {
    Grid,
    MenuItem
} from '@material-ui/core';
import {
    reduxForm,
    Field,
    FieldArray,
    Form,
    submit
} from 'redux-form';

import JudgeForm from './JudgeForm';

class NewPanelForm extends React.Component {

    state = {
        poolId: null
    };

    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.props.handleClose()
    };

    render() {
        return (
            <div>
                <Dialog
                    open={this.props.newQuestion}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <Grid
                        style={{
                            padding: 29,
                            fontSize: 27
                        }}
                    >
                        <span id="form-dialog-title">New Question</span>
                    </Grid>
                    <DialogContent>
                        <JudgeForm />
                    </DialogContent>
                    {/*<DialogActions>*/}
                        {/*<Button onClick={this.props.handleClose} color="primary">*/}
                            {/*Cancel*/}
                        {/*</Button>*/}
                        {/*<Button onClick={() => this.props.dispatch(submit('NewQuestionForm'))} color="primary">*/}
                            {/*Save*/}
                        {/*</Button>*/}
                    {/*</DialogActions>*/}
                </Dialog>
            </div>
        );
    }
}

export default connect()(NewPanelForm)