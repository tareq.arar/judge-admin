import React, { Component } from 'react';
import _ from 'lodash';
import axios from 'axios';
import {
    ENDPOINT
} from '../../config';
import {
    Grid,
    TableCell
} from '@material-ui/core';

import JudgeForm from './JudgeForm';
import Table from '../../components/TableComp';
import AssignExisting from "./AssignExisting";


const rows = [
    { id: 'name', numeric: false, disablePadding: true, label: 'Name' },
    { id: 'email', numeric: false, disablePadding: true, label: 'Email' },
];


class JudgesSection extends Component{

    state = {
        judges: {}
    };

    componentDidMount(){
        const token = localStorage.getItem('token');
        axios.get(`${ENDPOINT}/competitions/${this.props.compId}/judges`, {
            headers:{
                Accept: 'application/json',
                Authorization: `Bearer ${token}`
            }
        })
            .then(response =>{
                this.setState({
                    judges: response.data.data.judges,
                    panelId: response.data.data.panel_id
                })
            })
            .catch(error => {
                console.log(error)
            });
    }

    render(){
        const {
            judges,
            panelId,
        } = this.state;
        const {
            assignExisting
        } = this.props;
        return (
            <Grid
                style={{
                    margin: '10px 0'
                }}
            >
                <Grid
                    style={{
                        textAlign: 'center',
                    }}
                >
                    <JudgeForm
                        compId={this.props.compId}
                        panelId={this.props.panelId}
                    />
                </Grid>
                {
                    !_.isEmpty(judges) ?
                        _.map(this.state.judges, (judge) => {
                            return <Grid
                                style={{
                                    padding: 10,
                                    display: 'flex',
                                    justifyContent: 'space-between'
                                }}
                            >
                                <span>
                                    {judge.name}
                                    </span>
                                <span>
                                    {judge.email}
                                    </span>
                            </Grid>
                        })
                        :
                        null
                }
                {/*<AssignExisting*/}
                    {/*assignExisting={this.props.assignExisting}*/}
                    {/*handleClose={() => this.props.handleClose()}*/}
                    {/*compId={this.props.compId}*/}
                    {/*panelId={this.props.panelId}*/}
                {/*/>*/}
            </Grid>
        );
    }
}

export default JudgesSection;