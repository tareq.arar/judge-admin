import React, { Component } from 'react';
import {
    reduxForm,
    Field,
    FieldArray,
    Form
} from 'redux-form';

import {
    Grid,
    Button,
    IconButton
} from '@material-ui/core';
import {renderTextField} from "../../FormFields/index";
import {connect} from "react-redux";

import {ENDPOINT} from "../../config";
import axios from "axios/index";
import _ from 'lodash';

import Assignment from '@material-ui/icons/Assessment';


const validate = values => {
    const errors = {};
    const requiredFields = ["name", "email"];
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = "Required";
        }
    });
    return errors;
};


class JudgeForm extends Component{

    submit = (data) => {
        const token = localStorage.getItem('token');
        axios.post(`${ENDPOINT}/invitations/judge`,{
            name: data.name,
            email: data.email,
            competition_id: this.props.compId,
            panel_id: this.props.panelId
        },{
            headers:{
                Accept: 'application/json',
                Authorization: `Bearer ${token}`
            }
        })
            .then(response =>{
                this.setState({
                    judges: response.data.data.judges,
                    panelId: response.data.data.panel_id
                })
            })
            .catch(error => {
                console.log(JSON.stringify(error))
            });
    };

    render(){
        const { handleSubmit } = this.props;
        return(
            <Form
                onSubmit={handleSubmit(this.submit)}
            >
                <Grid style={{display: 'flex'}}>
                    <Grid
                        style={{
                            width: '100%',
                            display: 'flex',
                            justifyContent: 'space-between'
                        }}
                    >
                        <Field
                            id="name"
                            name={`name`}
                            label="Name"
                            component={renderTextField}
                        />
                        <Field
                            id="email"
                            type='email'
                            name={`email`}
                            label="email"
                            component={renderTextField}
                        />
                        <IconButton
                            type="submit"
                            aria-label="Assign"
                        >
                            <Assignment
                                style={{
                                    fontSize: 40
                                }}
                            />
                        </IconButton>
                    </Grid>
                </Grid>
            </Form>
        )
    }
}
export default connect()(reduxForm({
    form: 'NewJudge',
    validate,
    destroyOnUnmount: true
})(JudgeForm))