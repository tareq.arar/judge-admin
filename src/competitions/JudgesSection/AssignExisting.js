import React, { Component } from 'react';
import {
    Grid,
    Dialog,
    DialogContent,
    DialogActions,
    Button,
    IconButton
} from '@material-ui/core';

import Assignment from '@material-ui/icons/Assessment'

import _ from 'lodash';

import {ENDPOINT} from "../../config";
import axios from "axios/index";

const token = localStorage.getItem('token');


class AssignExisting extends Component{

    state = {
        judges: {}
    };

    assign = (judge) => {
        const token = localStorage.getItem('token');
        axios.post(`${ENDPOINT}/invitations/judge`,{
            name: judge.name,
            email: judge.email,
            competition_id: this.props.compId,
            panel_id: this.props.panelId
        },{
            headers:{
                Accept: 'application/json',
                Authorization: `Bearer ${token}`
            }
        })
            .then(response =>{
                this.setState({
                    judges: response.data.data.judges,
                    panelId: response.data.data.panel_id
                })
            })
            .catch(error => {
                console.log(JSON.stringify(error))
            });
    }

    componentDidMount(){
        axios.get(`${ENDPOINT}/judges`,
            {
                headers:{
                    Accept: 'application/json',
                    Authorization: `Bearer ${token}`
                }
            })
            .then(response => {
                this.setState({
                    judges: response.data.data
                });
            })
            .catch(error => {
                alert(JSON.stringify(error))
            });
    }

    render(){
        // email: "ahmad.hanandeh@zkfapps.com"
        // id: 2
        // image_url: null
        // name: "Ahmad Hanandeh"
        // updated_at: "2018-09-28 17:31:18"
        return (
            <div>
                <Dialog
                    open={this.props.assignExisting}
                    onClose={this.props.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <Grid
                        style={{
                            padding: 29,
                            fontSize: 27
                        }}
                    >
                        <span id="form-dialog-title">Assign Existing Judge</span>
                    </Grid>
                    <DialogContent>
                        {
                            _.map(this.state.judges, (judge) => {
                                return <Grid
                                    style={{
                                        padding: 10,
                                        display: 'flex',
                                        justifyContent: 'space-between',
                                        alignItems: 'center',
                                        borderBottom: '1px solid gray'
                                    }}
                                >
                                <span>
                                    {judge.name}
                                    </span>
                                    <span>
                                    {judge.email}
                                    </span>
                                    <IconButton
                                        aria-label="Assign"
                                        onClick={() => this.assign(judge)}
                                    >
                                        <Assignment
                                            style={{
                                                fontSize: 30
                                            }}
                                        />
                                    </IconButton>
                                </Grid>
                            })
                        }
                    </DialogContent>
                </Dialog>
            </div>
        );
    }
}

export default AssignExisting;