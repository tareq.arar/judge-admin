/**
 * Created by ahmadnasrawi on 8/20/17.
 */
import React from 'react';
// import _ from 'lodash';

// import DateTimeInput from '../components/DateTimePicker';
import {
    Datagrid,
    Edit,
    Create,
    FormTab,
    List,
    Filter,
    TabbedForm,
    TextInput,
    TextField,
    ReferenceInput,
    ImageInput,
    ImageField,
    RadioButtonGroupInput,
    SelectArrayInput,
    LongTextInput,
    AutocompleteInput,
    translate,
    // Delete,
    NumberInput,
    ReferenceField,
    DateField,
    EditButton,
    CardActions,
    CreateButton,
    ExportButton,
    RefreshButton,
} from 'react-admin';

import VerticalLinearStepper from './CompetitionList';


export const CompetitionsList = props => <VerticalLinearStepper /> ;
