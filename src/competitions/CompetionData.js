import React, { Component } from 'react';
import {
    Grid
} from '@material-ui/core';

import PanelsData from './PanelsData';
// import Questions from './Questions';
// import Competitiors from './Competitiors';

class CompetitionData extends Component{
    render(){
        const {competitionId} = this.props;
        return (
            <Grid>
                <PanelsData competitionId={competitionId} />
                {/*<Questions competitionId={competitionId} poolId={poolId} />*/}
                {/*<Competitiors competitionId={competitionId} />*/}
            </Grid>
        );
    }
}

export default CompetitionData;