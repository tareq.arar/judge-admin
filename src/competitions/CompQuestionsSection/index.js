import React, { Component } from 'react';

import {
    Grid,
    Button,
    CardContent,
    Card
} from '@material-ui/core'

import NewQuestionForm from './NewQuestionForm'
import QuestionsData from './CompCyestionsData';
import {ENDPOINT} from "../../config";
import axios from "axios/index";
import _ from 'lodash';

class CompQuestionSection extends Component{

    state = {
        newQuestion: false,
        questions: {}
    };

    getData = () => {
        const token = localStorage.getItem('token');
        axios.get(`${ENDPOINT}/competitions/${this.props.competition.id}`,
            {
                headers:{
                    Accept: 'application/json',
                    Authorization: `Bearer ${token}`
                }
            })
            .then(response => {
                let questions = response.data.data.questions;
                this.setState({
                    questions: questions
                })
            })
            .catch(error => {

            });
    };

    componentDidMount(){
        this.getData();
    }


    render(){
        return(
            <Grid
                style={{
                    marginTop: 30
                }}
            >
                <Grid
                    style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        width: '95%',
                        margin: '0 auto'
                    }}
                >
                    <Grid>
                        <span
                            style={{
                                fontSize: 30
                            }}
                        >
                            Questions
                        </span>
                    </Grid>
                    <Grid>
                        <Button
                            onClick={() => this.setState({newQuestion: true})}
                        >
                            Create New Question
                        </Button>
                    </Grid>
                </Grid>
                {
                    !_.isEmpty(this.props.competition.questions) || !_.isEmpty(this.state.questions) ?
                        <QuestionsData
                            reload={() => this.getData()}
                            rows={
                                !_.isEmpty(this.state.questions) ?
                                    this.state.questions :
                                    this.props.competition.questions
                            }
                        />
                        :
                        <Card>
                            <CardContent>
                                <span>There is No questions</span>
                            </CardContent>
                        </Card>
                }
                <NewQuestionForm
                    poolId={this.props.competition.pool_id}
                    newQuestion={this.state.newQuestion}
                    handleClose={() => {
                        this.setState({newQuestion: false})
                        this.getData();
                    }}
                />
            </Grid>

        );
    }
}

export default CompQuestionSection;