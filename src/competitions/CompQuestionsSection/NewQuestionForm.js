import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';

import {
    connect
} from 'react-redux';

import {
    Grid,
    MenuItem
} from '@material-ui/core';
import {
    reduxForm,
    Field,
    FieldArray,
    Form,
    submit
} from 'redux-form';

import {
    renderTextField,
    renderSelectField
} from '../../FormFields';
import {ENDPOINT} from "../../config";
import axios from "axios/index";
import {
    Slide
} from '@material-ui/core';
import _ from 'lodash';

const validate = values => {
    const errors = {};
    const requiredFields = ["question", "topic"];
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = "Required";
        }
    });
    return errors;
};

function Transition(props) {
    return <Slide direction="down" {...props} />;
}

class NewPanelForm extends React.Component {

    state = {
        poolId: null
    };
    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.props.handleClose()
    };

    submit = (data) => {
        const token = localStorage.getItem('token');
        axios.post(`${ENDPOINT}/criterias`, {
            question: data.question,
            topic: data.topic,
            type: data.type ? data.type : null,
            pool_id: this.props.poolId
        }, {
            headers: {
                Accept: 'application/json',
                Authorization: `Bearer ${token}`
            }
        }).then((response) => {
            this.handleClose()
        }).catch(error => {
            // this.setState({
            // })
        });
    };

    render() {
        const questionsTypes =[
            { id: 1, name: 'Yes/No' },
            { id: 2, name: '1 - 10' },
        ];
        const { handleSubmit } = this.props;
        return (
            <div>
                <Dialog
                    open={this.props.newQuestion}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                    TransitionComponent={Transition}
                >
                    <Grid
                        style={{
                            padding: 29,
                            fontSize: 27
                        }}
                    >
                        <span id="form-dialog-title">New Question</span>
                    </Grid>
                    <DialogContent>
                        <Grid
                            style={{
                                minWidth: 600
                            }}
                        >
                            Please fill in question details.
                        </Grid>
                        <Form
                            onSubmit={handleSubmit(this.submit)}
                        >
                                <Grid style={{width: '100%'}}>
                                    <Field
                                        id="question"
                                        name={`question`}
                                        label="Question"
                                        component={renderTextField}
                                        fullWidth
                                    />
                                </Grid>
                                <Grid style={{width: '100%'}}>
                                    <Field
                                        id="topic"
                                        name={`topic`}
                                        label="Topic"
                                        component={renderTextField}
                                        fullWidth
                                    />
                                </Grid>
                                <Grid style={{width: '100%'}}>
                                    <Field
                                        id="type"
                                        name={`type`}
                                        label="Question Type"
                                        component={renderSelectField}
                                        fullWidth
                                    >
                                        {_.map(questionsTypes, item => {
                                            return (
                                                <MenuItem
                                                    key={item.id}
                                                    value={item.id}
                                                >
                                                    {item.name}
                                                </MenuItem>
                                            );
                                        })}

                                    </Field>
                                </Grid>
                        </Form>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.props.handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={() => this.props.dispatch(submit('NewQuestionForm'))} color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default connect()(reduxForm({
    form: 'NewQuestionForm',
    validate,
    destroyOnUnmount: true
})(NewPanelForm))