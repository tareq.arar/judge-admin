import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {ENDPOINT} from "../../config";
import axios from "axios/index";
import CircularProgress from "@material-ui/core/CircularProgress";
import Slide from '@material-ui/core/Slide';



import {
    IconButton,
    Dialog,
    DialogContent,
    Grid,
    DialogActions,
    Button
} from '@material-ui/core';

import Delete from '@material-ui/icons/Delete';

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
});

let id = 0;

const token = localStorage.getItem('token');

function Transition(props) {
    return <Slide direction="up" {...props} />;
}


class SimpleTable extends React.Component {

    state ={
        disable: false,
        confirm: false
    }

    deleteQuestion = () => {
        axios.delete(`${ENDPOINT}/criterias/${this.state.toBeDeleted}`,{
            headers: {
                Accept: 'application/json',
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => {
                this.setState({
                    confirm: false,
                    toBeDeleted: 0
                })
                this.props.reload()
            }).catch(error => {
            console.log(error)
        })
    };

    confirmDelete =(id) => {
        this.setState({
            confirm: true,
            toBeDeleted: id
        })
    }

    render(){
        const { classes, rows } = this.props;

        return (
            <Paper className={classes.root}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell>Topic</TableCell>
                            <TableCell>Question</TableCell>
                            <TableCell>Type</TableCell>
                            <TableCell>Actions</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map(row => {
                            return (
                                <TableRow key={row.id}>
                                    <TableCell component="th" scope="row">
                                        {row.topic}
                                    </TableCell>
                                    <TableCell >{row.question}</TableCell>
                                    <TableCell >{row.type === 1 ? 'Yes/NO' : '1 to 10'}</TableCell>
                                    <TableCell>
                                        <IconButton
                                            aria-label="Delete"
                                            onClick={() => this.confirmDelete(row.id)}
                                        >
                                            <Delete
                                                style={{
                                                    color: 'red'
                                                }}
                                            />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
                <Dialog
                    fullScreen={false}
                    open={this.state.confirm}
                    onClose={() => this.setState({confirm: false})}
                    aria-labelledby="responsive-dialog-title"
                    TransitionComponent={Transition}
                >
                    <DialogContent>
                        <Grid
                            style={{
                                padding: 20
                            }}
                        >
                            <span>Are You sure you want to delete this question?</span>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.deleteQuestion} color="primary">
                            {this.state.disable ? <CircularProgress size={20} /> : ""}
                            &nbsp; yes
                        </Button>
                        <Button onClick={() => this.setState({confirm: false})} color="primary" autoFocus>
                            No
                        </Button>
                    </DialogActions>
                </Dialog>
            </Paper>
        );
    }
}

SimpleTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleTable);
