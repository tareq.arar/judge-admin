import React, { Component } from 'react';
import {ENDPOINT} from "../../config";
import axios from "axios/index";
import _ from 'lodash';

import {
    Dialog,
    Grid,
    DialogContent,
    withStyles,
    Button,
    CircularProgress
} from '@material-ui/core';
import PropTypes from 'prop-types';

import { styles } from './styles';


const token = localStorage.getItem('token');


class ExistingCompetitor extends Component{

    state = {
        competitors: {},
        disable: false
    };

    getData = () => {
        axios.get(`${ENDPOINT}/competitors`,
            {
                headers:{
                    Accept: 'application/json',
                    Authorization: `Bearer ${token}`
                }
            })
            .then(response => {
                let competitors = response.data.data;
                this.setState({
                    competitors: competitors
                })
            })
            .catch(error => {

            });
    };
    addToComp = (competitorId) => {
        this.setState({
            disable: true
        });
        axios.post(`${ENDPOINT}/competitions/${this.props.competitionId}/competitors`,{
            competitor_id: competitorId,
            panel_id: this.props.panelId
        },{
                headers:{
                    Accept: 'application/json',
                    Authorization: `Bearer ${token}`
                }
            })
            .then(response => {
                this.setState({
                    disable: false
                })
                this.props.refresh()
            })
            .catch(error => {

            });
    }

    componentDidMount(){
        this.getData();
    }

    render(){
        const {
            classes
        } = this.props;
        return (
            <div
                style={{
                    minWidth: '90%'
                }}
            >
                <Dialog
                    open={this.props.addCompetitor}
                    onClose={this.props.handleClose}
                    aria-labelledby="form-dialog-title"
                    classes={{
                        paperWidthSm: classes.root
                    }}
                >
                    <Grid
                        style={{
                            padding: 29,
                            fontSize: 27
                        }}
                    >
                        <span id="form-dialog-title">Add competitor to competition</span>
                    </Grid>
                    <DialogContent>
                        {
                            _.map(this.state.competitors, (competitor) => {
                                return <Grid style={{
                                    width: '100%',
                                    borderBottom: '1px solid gray',
                                    display: 'flex',
                                    justifyContent: 'space-evenly',
                                    alignItems: 'center'
                                }}>
                                    <Grid
                                        style={{
                                            padding: 5,
                                            display: 'flex',
                                            alignItems: 'center',
                                            width: '90%',
                                            marginBottom: 10
                                        }}
                                    >
                                        <Grid>
                                            <img src={competitor.media} width="75"/>
                                        </Grid>
                                        <Grid
                                            style={{
                                                marginLeft: 10
                                            }}
                                        >
                                            <Grid>
                                                <Grid>
                                                    <span
                                                        style={{
                                                            fontSize: 25
                                                        }}
                                                    >
                                                        {competitor.name}</span>
                                                </Grid>
                                            </Grid>
                                            <Grid>
                                                <span
                                                    style={{
                                                        display: 'block',
                                                        fontSize: 10
                                                    }}
                                                >
                                                    Mobile number: {competitor.mobile}</span>
                                                {
                                                    competitor.founders.length > 0 ?
                                                        <span
                                                            style={{
                                                                display: 'block',
                                                                fontSize: 10
                                                            }}
                                                        >
                                                            Founders: {competitor.founders.join(' And ')}
                                                            </span>
                                                        :
                                                        null
                                                }
                                                {
                                                    competitor.industry ?
                                                            <span
                                                                style={{
                                                                    display: 'block',
                                                                    fontSize: 10
                                                                }}
                                                            >
                                                                Industry: {competitor.industry}</span>
                                                        :
                                                        null
                                                }
                                            </Grid>
                                            <Grid
                                                style={{
                                                    width: '75%'
                                                }}
                                            >
                                                <span>{competitor.description}</span>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    <Grid>
                                        <Button
                                            onClick={() => this.addToComp(competitor.id)}
                                        >
                                            {this.state.disable ? <CircularProgress size={20} /> : ""}
                                            &nbsp; Add

                                        </Button>
                                    </Grid>
                                </Grid>
                            })
                        }
                    </DialogContent>
                </Dialog>
            </div>
        );
    }
}

ExistingCompetitor.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ExistingCompetitor);