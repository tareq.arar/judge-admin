export const styles ={
    root:{
      minWidth: '90%'
    },
    rootWithDesktop:{
      minWidth: '40%'
    },
    titleContainer:{
        textAlign: 'center',
        padding: 50,
        fontSize:24
    },
    closeIcon:{
        padding: '10px 18px',
        position: 'absolute',
        right: 0,
        color: '#979797'
    },
    dialogActions:{
        padding: 25,
        justifyContent: 'center'
    },
    primeryButton:{
        backgroundImage: 'linear-gradient(to right, #24D5C9, #24D5C9)',
        color: 'white',
        textTransform: 'capitalize',
        width: 100,
        fontWeight: 'bold'
    }
};