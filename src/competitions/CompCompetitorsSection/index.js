import React, { Component } from 'react';

import {
    Grid,
    Button,
    Card,
    CardContent
} from '@material-ui/core'

import NewCompetitorForm from './NewCompetitorForm'
import ExistingCompetitor from './AddExistingCompetitor';
import {ENDPOINT} from "../../config";
import axios from "axios/index";
import _ from 'lodash';
import CompetitorsData from './CompCompetitorData';

class CompCompetitorsSection extends Component{

    state = {
        newCompetitor: false,
        addCompetitor: false,
        competitors: {}
    };
    getData = () => {
        const token = localStorage.getItem('token');
        axios.get(`${ENDPOINT}/competitions/${this.props.competition.id}/competitors`, {
            headers: {
                Accept: 'application/json',
                Authorization: `Bearer ${token}`
            }
        }).then((response) => {
            this.setState({
                competitors: !_.isEmpty(response.data.data.competitors) ? response.data.data.competitors : {}
            })
        }).catch(error => {
            // this.setState({
            // })
        });
    };
    componentDidMount(){
        this.getData()
    }

    render(){
        return(
            <Grid
                style={{
                    marginTop: 30
                }}
            >
                <Grid
                    style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        width: '95%',
                        margin: '0 auto'
                    }}
                >
                    <Grid>
                        <span
                            style={{
                                fontSize: 30
                            }}
                        >
                            Competitors
                        </span>
                    </Grid>
                    <Grid
                        style={{
                            display: 'flex',
                            justifyContent: 'space-between',
                            alignItems: 'center'
                        }}
                    >
                        <Button
                            onClick={() => this.setState({addCompetitor: true})}
                        >
                            Add Competitor
                        </Button>
                        OR
                        <Button
                            onClick={() => this.setState({newCompetitor: true})}
                        >
                            Create New Competitor
                        </Button>
                    </Grid>
                </Grid>
                {
                    !_.isEmpty(this.state.competitors) ?
                        <Grid>
                            <CompetitorsData rows={this.state.competitors}/>
                        </Grid>
                        :
                        <Card>
                            <CardContent>
                                <span>There is No Competitors</span>
                            </CardContent>
                        </Card>
                }
                <ExistingCompetitor
                    panelId={this.props.panelId}
                    addCompetitor={this.state.addCompetitor}
                    competitionId={this.props.competition.id}
                    refresh={() => this.getData()}
                    handleClose={() => {
                        this.setState({addCompetitor: false})}}
                />
                <NewCompetitorForm
                    panelId={this.props.panelId}
                    newQuestion={this.state.newCompetitor}
                    competitionId={this.props.competition.id}
                    handleClose={() => {
                        this.getData();
                        this.setState({newCompetitor: false})}}
                />
            </Grid>

        );
    }
}

export default CompCompetitorsSection;