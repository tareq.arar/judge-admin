import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';

import ChipInput from 'material-ui-chip-input'

import {
    connect
} from 'react-redux';

import {
    Grid,
    MenuItem
} from '@material-ui/core';
import {
    reduxForm,
    Field,
    FieldArray,
    Form,
    submit
} from 'redux-form';

import {
    renderTextField,
    renderSelectField, renderMultiLineField, renderChipField
} from '../../FormFields';
import {ENDPOINT} from "../../config";
import axios from "axios/index";
import _ from 'lodash';

const validate = values => {
    const errors = {};
    const requiredFields = ["name", "description"];
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = "Required";
        }
    });
    return errors;
};



class NewCompetitorForm extends React.Component {

    state = {
        poolId: null,
        founders: []
    };
    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.props.handleClose()
    };

    submit = (data) => {
        const token = localStorage.getItem('token');
        let founders = this.state.founders.length === 0 ? "" : this.state.founders.join(",")
        axios.post(`${ENDPOINT}/competitors`, {
            name: data.name,
            description: data.description,
            founders: founders,
            mobile: data.mobile,
            industry: data.industry,
            launch_date: this.props.launch_date
        }, {
            headers: {
                Accept: 'application/json',
                Authorization: `Bearer ${token}`
            }
        }).then((response) => {
            axios.post(`${ENDPOINT}/competitions/${this.props.competitionId}/competitors`, {
                competitor_id: response.data.data.id,
                panel_id: this.props.panelId,
            }, {
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${token}`
                }
            }).then((response) => {
                this.handleClose()
            }).catch(error => {
                // this.setState({
                // })
            });
        }).catch(error => {
            // this.setState({
            // })
        });
    };

    render() {
        const { handleSubmit } = this.props;
        return (
            <div>
                <Dialog
                    open={this.props.newQuestion}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                >
                    <Grid
                        style={{
                            padding: 29,
                            fontSize: 27
                        }}
                    >
                        <span id="form-dialog-title">New Competitor</span>
                    </Grid>
                    <DialogContent>
                        <Grid
                            style={{
                                minWidth: 600
                            }}
                        >
                            Please fill in competitor details.
                        </Grid>
                        <Form
                            onSubmit={handleSubmit(this.submit)}
                        >
                            <Grid style={{width: '100%'}}>
                                <Field
                                    id="name"
                                    name={`name`}
                                    label="Name"
                                    component={renderTextField}
                                    fullWidth
                                />
                            </Grid>
                            <Grid>
                                <Field
                                    name="description"
                                    type="text"
                                    component={renderMultiLineField}
                                    label="Description"
                                    fullWidth
                                />
                            </Grid>
                            <Grid style={{width: '100%'}}>
                                <ChipInput
                                    onChange={(chips) => this.setState({founders: chips})}
                                />
                            </Grid>
                            <Grid style={{width: '100%'}}>
                                <Field
                                    id="mobile"
                                    name={`mobile`}
                                    label="Mobile Number"
                                    component={renderTextField}
                                    fullWidth
                                />
                            </Grid>
                            <Grid style={{width: '100%'}}>
                                <Field
                                    id="industry"
                                    name={`industry`}
                                    label="Industry"
                                    component={renderTextField}
                                    fullWidth
                                />
                            </Grid>
                            <Grid style={{width: '100%'}}>
                                <Field
                                    type="date"
                                    name="launch_date"
                                    component={renderTextField}
                                    label="Launch Date"
                                    defaultValue="2018-05-24T10:30"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </Grid>
                        </Form>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.props.handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={() => this.props.dispatch(submit('NewCompetitorForm'))} color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default connect()(reduxForm({
    form: 'NewCompetitorForm',
    validate,
    destroyOnUnmount: true
})(NewCompetitorForm))