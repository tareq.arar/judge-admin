import React, { Component } from 'react';
import axios from 'axios';
import _ from 'lodash';
import {
    ENDPOINT
} from "../config";

import {
    Grid,
    Button,
    Modal,
    Select,
    Input,
    MenuItem,
    Chip,
    FormControl
} from '@material-ui/core';

class PanelsData extends Component{
    state = {
        judges: {},
        panelsData: {},
        open: false,
        cureentPanelId: 0,
        judgesSelect: []
    }

    componentDidMount(){
        const token = localStorage.getItem('token');
        axios.get(`${ENDPOINT}/competitions/${this.props.competitionId}/judges`,{
            headers:{
                Accept: 'application/json',
                Authorization: `Bearer ${token}`
            }
        }).then(response => {
            let alldata = {};
            let panels = [];
            let panelsData = [];
            _.map(response.data.data.judges, (judge) => {
                if(!alldata[judge.panel_id])
                    alldata[judge.panel_id] = [];
                alldata[judge.panel_id].push(judge);
                if(!_.includes(panels, judge.panel_id))
                    panels.push(judge.panel_id)
            });
            _.map(panels, (panel) => {
                axios.get(`${ENDPOINT}/panels/${panel}`,
                    {
                        headers:{
                            Accept: 'application/json',
                            Authorization: `Bearer ${token}`
                        }
                    }
                ).then(response => {
                    console.log(response)
                    let panelsResponse = response.data.data;
                    panelsData.push({
                        panel_id: panelsResponse.id,
                        panel_name: panelsResponse.name,
                    })
                    axios.get(`${ENDPOINT}/judges`,
                        {
                            headers:{
                                Accept: 'application/json',
                                Authorization: `Bearer ${token}`
                            }
                        })
                        .then(response => {
                            this.setState({
                                judges: alldata,
                                panelsData: panelsData,
                                allJudges: response.data.data
                            })
                        })
                        .catch(error => {
                            console.log(error)
                        });
                }).catch(error => {
                    console.log(error)
                });
            })

        }).catch(error => {
            console.log(error)
        });
    }

    handleChange = (event)=>{
        this.setState({judgesSelect:event.target.value});
    };

    updatePanel = () => {
        console.log();
    };

    render(){
        const { panelsData, judges } = this.state;
        console.log(judges)
        return (
            <Grid
                style={{
                    width: '93%',
                    margin: '0 auto'
                }}
            >
                {
                    _.map(panelsData, (panel, key) => {
                        return(
                                <Grid
                                    key={key}
                                    style={{
                                        borderRadius: 5,
                                        maxWidth: 500,
                                        minWidth: 500,
                                        border: '1px solid gray',
                                        display: 'inline-block',
                                        minHeight: 150,
                                        padding: 13,
                                    }}
                                >
                                    <Grid
                                        style={{
                                            display: 'flex',
                                            justifyContent: 'space-between',
                                            alignItems: 'center'
                                        }}
                                    >
                                    <span
                                        style={{
                                            fontSize: 30
                                        }}
                                    >
                                        {panel.panel_name}
                                    </span>
                                        <Button
                                            onClick={() => {
                                                let ids = [];
                                                _.map(judges[panel.panel_id], (judge) => {
                                                   ids.push(judge.id);
                                                });
                                                this.setState({
                                                    judgesSelect: ids,
                                                    cureentPanelId: panel.panel_id,
                                                    open: true
                                                })
                                            }}
                                        >
                                            Edit
                                        </Button>
                                    </Grid>
                                    <Grid
                                        style={{
                                            overflow: 'auto',
                                            minHeight: 100
                                        }}
                                    >
                                        {
                                            _.map(judges[panel.panel_id], (judge) => {
                                                return (
                                                    <Grid>
                                                        <span>{judge.name}</span>
                                                    </Grid>
                                                )
                                            })
                                        }
                                    </Grid>
                                </Grid>
                        )
                    })
                }

                {
                    this.state.open ?
                        <Modal
                            aria-labelledby="simple-modal-title"
                            aria-describedby="simple-modal-description"
                            open={true}
                            onClose={() => this.setState({
                                open: false
                            })}
                        >
                            <div
                                style={{
                                    position: 'absolute',
                                    width: 500,
                                    top: `50%`,
                                    left: `50%`,
                                    backgroundColor: 'white',
                                    transform: `translate(-50%, -50%)`,
                                    padding: 50,
                                    outline: 'none',
                                    maxHeight: 450,
                                    overflow: 'auto'
                                }}
                            >
                                <h1>
                                    {_.find(panelsData,{panel_id: this.state.cureentPanelId}).panel_name}
                                </h1>
                                {console.log(this.state.judgesSelect, 'fsklfjklsdfhsdhfkjhsdfkh')}
                                <Grid>
                                    <Select
                                        multiple
                                        style={{
                                            width: '100%'
                                        }}
                                        value={this.state.judgesSelect}
                                        onChange={this.handleChange}
                                        input={<Input id="select-multiple-chip" />}
                                        renderValue={selected => {
                                            console.log(selected)
                                            return(
                                                <div>
                                                    {selected.map(value => (
                                                        <Chip
                                                            key={value}
                                                            label={_.find(this.state.allJudges, {id: value}).name}
                                                        />
                                                    ))}
                                                </div>
                                            )}
                                        }
                                    >
                                        {console.log(this.state.allJudges)}
                                        {!_.isEmpty(this.state.allJudges)?
                                            this.state.allJudges.map(judge => (
                                                <MenuItem key={judge.id} value={judge.id} >
                                                    {judge.name}
                                                </MenuItem>
                                            )) :
                                            <MenuItem>
                                                no data
                                            </MenuItem>
                                        }
                                    </Select>
                                </Grid>
                                <Button
                                    style={{
                                        float: 'right'
                                    }}
                                    onClick={this.updatePanel}
                                >
                                    Save
                                </Button>

                            </div>
                        </Modal>
                        :
                        null
                }
            </Grid>
        );
    }
}

export default PanelsData;