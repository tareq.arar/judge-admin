import React, { Component } from 'react';
import {
    connect
} from 'react-redux';

import PropTypes from "prop-types";
import { withRouter } from "react-router";
import CircularProgress from "@material-ui/core/CircularProgress";

import {
    ENDPOINT
} from '../config';
import axios from 'axios';
import _ from 'lodash';

import NewCompetition from './NewCompetitionComponent';
import FullScreenDialog from '../components/SearchDialog';

import {
    Card,
    CardContent,
    CardHeader,
    Grid,
    Typography,
    IconButton,
    Collapse,
    Button,
    Modal
} from '@material-ui/core';

import CompetitionComp from '../components/CompetitionComp';

class CompetitionList extends Component{
    constructor(props){
        super(props)
        this.state = {
            competitions: {},
            openNew: false,
            activeStep: 0,
            loading: true
        }
    }

    getCompitions = () =>{
        const token = localStorage.getItem('token');
        axios.get(`${ENDPOINT}/competitions`,{
            headers:{
                Accept: 'application/json',
                Authorization: `Bearer ${token}`
            }
        })
            .then((response) => {
                this.setState({
                    competitions: response.data.data,
                    loading: false
                })
                if(_.isEmpty(response.data.data)){
                    this.setState({
                        loading: false,
                    })
                }
            })
            .catch(error => {
                console.log(error)
            });
    }

    handleNext = () => {
        this.setState({activeStep: (this.state.activeStep + 1)})
    }

    componentDidMount(){
        this.getCompitions();
    }
    openModal = () => {
        this.setState({
            openNew: true,
        })
    }

    render(){
        const {
            competitions
        } = this.state
        return(
        <Grid>
            {
                this.state.loading ?
                    <Grid
                        style={{
                            position: 'relative'
                        }}
                    >
                        <CircularProgress
                            style={{
                                position: 'absolute',
                                right: '50%'
                            }}
                            size={50}
                        />
                    </Grid>
                    :
                    null
            }
            <Grid
                style={{
                    textAlign: 'right',
                    textTransform: 'capitalize'
                }}
            >
                <Button
                    style={{
                        color: '#2196f3',
                        textTransform: 'capitalize'
                    }}
                    onClick={() => this.setState({openNew: true})}
                >
                    New Competition
                </Button>
            </Grid>

            {
                !_.isEmpty(competitions) ?
                _.map(competitions, (competition, key) =>{
                    return(
                        <CompetitionComp
                            reload={() =>
                                this.getCompitions()
                            }
                            key={key}
                            competition={competition}
                        />
                    )
                })
                    :
                    <Grid
                        style={{
                            textAlign: 'center',
                            fontSize: 30
                        }}
                    >
                        No Competitions
                    </Grid>
            }
            {
                this.state.openNew ?
                    <NewCompetition
                        created={(competitionDetails) => {
                        this.setState({openNew: false})
                        this.props.history.push({
                            pathname: '/competition-details',
                            state: {competitionData: competitionDetails}
                        })
                    }}
                        close={() => {
                            this.setState({
                                openNew: false
                            })
                            this.getCompitions()
                        }}
                    />
                    :
                    null
            }
            </Grid>
        )
    }
}


CompetitionList.propTypes = {
    history: PropTypes.object.isRequired
};


export default withRouter(connect()(CompetitionList));