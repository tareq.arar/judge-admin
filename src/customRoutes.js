import React from 'react';
import { Route } from 'react-router-dom';

import CompetitionDetails from './competitions/CompetitionDetails';
export default [
    <Route exact path="/competition-details" component={CompetitionDetails} />,
];