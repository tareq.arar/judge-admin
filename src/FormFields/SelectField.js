
import React from 'react';
import {
    FormControl,
    InputLabel,
    Select,
    FormHelperText
} from '@material-ui/core';

export const renderSelectField = ({
                               input,
                               label,
                               styles,
                               meta: { touched, error },
                               children,
                               ...custom
                           }) => (
    <FormControl error={error && touched}
                 style={{
                     width: '100%'
                 }}
    >
        <InputLabel
            error={error && touched}
            htmlFor={input.id}
        >
            {label}
        </InputLabel>
        <Select {...input} children={children} {...custom}
                classes={{
                    root: {
                        width: "100%",
                        padding: 10,
                        fontSize: 15
                    },
                }}
        />
        {error && touched ? (
            <FormHelperText >{error}</FormHelperText>
        ) : (
            ""
        )}
    </FormControl>
);