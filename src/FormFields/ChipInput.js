import React from 'react';
import {
    TextField
} from '@material-ui/core';
import ChipInput from 'material-ui-chip-input'
export const renderChipField = ({
                                    input,
                                    label,
                                    meta: { touched, error },
                                    ...custom
                                }) => (
    <ChipInput
        label={label}
        error={error && touched}
        InputProps={{
            name: input.name
        }}
    />

);
