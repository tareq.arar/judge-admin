import React from 'react';
import {
    TextField
} from '@material-ui/core';
export const renderTextField = ({
                             input,
                             label,
                             meta: { touched, error },
                             ...custom
                         }) => (
    <TextField
        label={label}
        error={error && touched}
        helperText={touched && error}
        {...input}
        {...custom}
    />
);
