import React from 'react';
import {
    TextField
} from '@material-ui/core';

export const renderMultiLineField = ({
                                  input,
                                  label,
                                  meta: { touched, error },
                                  ...custom
                              }) => (
    <TextField
        id="standard-multiline-static"
        label={label}
        error={error && touched}
        helperText={touched && error}
        multiline
        rows="4"
        defaultValue="Default Value"
        margin="normal"
        {...input}
        {...custom}
    />
);
