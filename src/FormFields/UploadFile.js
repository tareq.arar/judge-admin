import React from 'react';
export const UploadFile = ({ input: { value: omitValue, ...inputProps }, meta: omitMeta, ...props }) => (
    <div class="upload-btn-wrapper">
        <button class="btn">{props.label}</button>
        <input
            style={{
                marginLeft: 30,
            }}
            type="file"
            {...inputProps}
            {...props}
        />
    </div>
);