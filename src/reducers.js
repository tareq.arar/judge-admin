import {
    STEP_THREE,
    FIRST_STEP,
    FORTH_STEP
} from './actions';

const INIT_STATE = {
    initialValues: {},
};

export default (state = INIT_STATE, { type, payload }) => {
    if (type === STEP_THREE) {
        return {initialValues: payload.initialValues};
    } else if(type === FIRST_STEP){
        return {initialValues: payload.initialValues};
    } else if(type === FORTH_STEP){
        return {initialValues: payload.initialValues};
    }
    return state;
}