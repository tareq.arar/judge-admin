import React, { Component } from 'react';
import { Admin, Resource } from 'react-admin';

import restClint from './restClient';
import './App.css';
import LoginPage from './LoginForm';

import { PoolsList, PoolCreate, PoolEdit } from './pools';
import { CompetitorsList, CompetitorCreate, CompetitorEdit } from './competitors';
import { CriteriasList, CriteriasCreate, CriteriaEdit } from './criterias';
import {
    CompetitionsList
} from './competitions';
import {
    JudgeCreate,
    JudgesList,
    JudgeEdit
} from './judges';

import authProvider from './authProvider';
import customRoutes from './customRoutes'
import createHistory from 'history/createBrowserHistory';

import reducers from './reducers';

const history = createHistory();

class App extends Component {
  render() {
    return (
        <Admin
            appTitle="Zain Judge Admin"
            authProvider={authProvider}
            loginPage={LoginPage}
            dataProvider={restClint}
            customRoutes={customRoutes}
            customReducers={{reducers}}
            history={history}
        >
            <Resource
                name="competitions"
                list={CompetitionsList}
                options={{ label: 'Competitions' }}
            />
            <Resource
                name="pools"
                list={PoolsList}
                create={PoolCreate}
                edit={PoolEdit}
                options={{ label: 'Pools' }}
            />
            <Resource
                name="judges"
                list={JudgesList}
                create={JudgeCreate}
                edit={JudgeEdit}
                options={{ label: 'Judges' }}
            />
            <Resource
                name="competitors"
                list={CompetitorsList}
                create={CompetitorCreate}
                edit={CompetitorEdit}
                options={{ label: 'Competitors' }}
            />
            <Resource
                name="criterias"
                edit={CriteriaEdit}
                list={CriteriasList}
                create={CriteriasCreate}
                options={{ label: 'Criterias (Question)' }}
            />
        </Admin>
    );
  }
}

export default App;