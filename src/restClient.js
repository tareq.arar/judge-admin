import {
    GET_LIST,
    GET_ONE,
    GET_MANY,
    GET_MANY_REFERENCE,
    CREATE,
    UPDATE,
    DELETE,
    DELETE_MANY
} from 'react-admin';

import _ from 'lodash';
// import moment from 'moment';


import {
    ENDPOINT,
} from './config';

import { fetchJson, queryParameters } from './fetchdata';

const API_URL = ENDPOINT;

/**
 * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
 * @param {String} resource Name of the resource to fetch, e.g. 'posts'
 * @param {Object} params The REST request params, depending on the type
 * @returns {Object} { url, options } The HTTP request parameters
 */
let URLs = null,
    n = 0;
const withFilterString = ['adtones', 'banners', 'features', 'campaigns', 'fixtures'];
const convertRESTRequestToHTTP = (type, resource, params) => {
    let url = '';
    const options = {};
    const token = localStorage.getItem('token');

    options.headers = new Headers({ Accept: 'application/json', 'content-type': 'application/json',
        Authorization: `Bearer ${token}` });
    switch (type) {
    case GET_LIST: {
        const { page, perPage } = params.pagination;
        const { field, order } = params.sort;
        if (typeof params.filter.q === 'undefined') {
            params.filter.q = '';
        }
        const query = {
            sort: JSON.stringify([field, order]),
            range: JSON.stringify([(page - 1) * perPage, page * perPage - 1]),
            page,
            q: params.filter.q,
        };
        const SortingParts = JSON.parse(query.sort);
            // Finding filter keys
        const FilterKeys = Object.keys(params.filter);
            // Finding filter values
        const KeyValues = Array.from(Object.keys(params.filter), k => params.filter[k]);
        let Filters = '';
        console.info('is with filter string', _.includes(withFilterString, resource.trim()));
        for (let index = 0; index < FilterKeys.length - 1; index++) {
                // Fix Date format
            if (FilterKeys[index].includes('date')) {
                const r_date = new Date(KeyValues[index].split('T')[0]);
                r_date.setDate(r_date.getDate() + 1);
                if (_.includes(withFilterString, resource.trim()) && FilterKeys[index] !== 'q') {
                    Filters = `${`${Filters}filter_${FilterKeys[index]}`}=${r_date.toISOString().slice(0, 10)}&`;
                } else {
                    Filters = `${Filters + FilterKeys[index]}=${r_date.toISOString().slice(0, 10)}&`;
                }
            } else if (_.includes(withFilterString, resource.trim()) && FilterKeys[index] !== 'q') {
                Filters = `${`${Filters}filter_${FilterKeys[index]}`}=${KeyValues[index]}&`;
            } else {
                Filters = `${Filters + FilterKeys[index]}=${KeyValues[index]}&`;
            }
        }
        url = `${API_URL}/${resource}?${queryParameters(query)}&${Filters}&field=${SortingParts[0]}&sortingOrder=${SortingParts[1]}`;
        break;
    }
    case GET_ONE:
        url = `${API_URL}/${resource}/${params.id}`;
        break;
    case GET_MANY: {
            /*  const query = {
                  filter: JSON.stringify({ id: params.ids }),
              };*/
        url = `${API_URL}/${resource}`;
        break;
    }
    case GET_MANY_REFERENCE: {
        const { page, perPage } = params.pagination;
        const { field, order } = params.sort;
        const featureId = '';
        // if(resource.trim() === 'features'){
        //     if(params.target){
        //         featureId = `/${params.id}`;
        //     }
        // }
        const query = {
            sort: JSON.stringify([field, order]),
            range: JSON.stringify([(page - 1) * perPage, (page * perPage) - 1]),
            filter: JSON.stringify({ ...params.filter, [params.target]: params.id }),
        };

        url = `${API_URL}/${resource}${featureId}?${queryParameters(query)}`;
        break;
    }
    case UPDATE:
        url = `${API_URL}/${resource}/${params.id}`;
        var formDatas = new FormData();

        if (resource.trim() === 'tones') {
            options.method = 'POST';
            options.headers = new Headers({ Accept: 'application/json', Authorization: `Bearer ${token}` });
            options.mimeType = 'multipart/form-data';
            formDatas.append('name', params.data.name);
            formDatas.append('name_arabic', params.data.name_arabic);

            if (typeof params.data.cover_image !== 'undefined') { formDatas.append('cover_image', params.data.cover_image[0]); }

            if (params.data.category_id == null) { formDatas.append('category_id', null); } else { formDatas.append('category_id', params.data.category_id); }

            if (params.data.parent !== null) { formDatas.append('parent_tone_id', params.data.parent.id); }

            formDatas.append('author_id', params.data.author.id);

            if (params.data.genres.length === 0) { formDatas.append('genre_id', null); } else { formDatas.append('genre_id', params.data.genres); }

            formDatas.append('publisher_id', params.data.publisher.id);

            formDatas.append('language', params.data.language);

            if (params.data.album_name !== null) { formDatas.append('album_name', ''); } else { formDatas.append('album_name', params.data.album_name); }

            if (params.data.tags.length !== 0) { formDatas.append('tags', params.data.tags); }
            options.body = (formDatas);
        } else {
            options.method = 'PATCH';
            options.body = JSON.stringify(params.data);
        }

        break;
    case CREATE:
        url = `${API_URL}/${resource}`;
        options.method = 'POST';
        var formData = new FormData();

        if (resource.trim() === 'criterias') {
            options.headers = new Headers({ Accept: 'application/json', Authorization: `Bearer ${token}` });
            options.mimeType = 'multipart/form-data';
            formData.append('question', params.data.question);
            formData.append('type', params.data.type);
            formData.append('topic', params.data.topic ? params.data.topic : '');
            formData.append('pool_id', params.data.pool_id);
            options.body = (formData);
        } else if (resource.trim() === 'competitors') {
            options.headers = new Headers({ Accept: 'application/json', Authorization: `Bearer ${token}` });
            options.mimeType = 'multipart/form-data';
            formData.append('name', params.data.name);
            formData.append('description', params.data.description);
            formData.append('founders', params.data.founders ? params.data.founders : '');
            formData.append('mobile', params.data.mobile);
            formData.append('launch_date', params.data.launch_date);
            formData.append('industry', params.data.industry);
            options.body = (formData);
        } else if (resource.trim() === 'panels') {
            options.headers = new Headers({ Accept: 'application/json', Authorization: `Bearer ${token}` });
            options.mimeType = 'multipart/form-data';
            formData.append('name', params.data.name);
            formData.append('room', params.data.room);
            options.body = (formData);
        } else if (resource.trim() === 'pools') {
            options.headers = new Headers({ Accept: 'application/json', Authorization: `Bearer ${token}` });
            options.mimeType = 'multipart/form-data';
            formData.append('name', params.data.name);
            formData.append('score_unit', params.data.score_unit);
            options.body = (formData);
        } else if (resource.trim() === 'judges') {
            options.headers = new Headers({ Accept: 'application/json', Authorization: `Bearer ${token}` });
            options.mimeType = 'multipart/form-data';
            formData.append('name', params.data.name);
            formData.append('email', params.data.email);
            formData.append('image_url', params.data.image_url);
            options.body = (formData);
        }
        break;
    case DELETE:
        url = `${API_URL}/${resource}/${params.id}`;
        options.method = 'DELETE';
        break;
    case DELETE_MANY:
        console.log(params)
        url = `${API_URL}/${resource}/${params.ids}`;
        options.method = 'DELETE';
        break;
    default:
        throw new Error(`Unsupported fetch action type ${type}`);
    }

    return { url, options };
};

function ConvertStringToDate(StringDate) {
    const dateString = StringDate;
    const date = new Date(dateString);
    const yr = date.getFullYear();
    let mo = date.getMonth() + 1;
    let day = date.getDate();
    if (day < 10) { day = `0${day}`; }
    if (mo < 10) { mo = `0${mo}`; }
    const hours = date.getHours();
    const hr = hours < 10 ? `0${hours}` : hours;

    const minutes = date.getMinutes();
    const min = (minutes < 10) ? `0${minutes}` : minutes;

    const seconds = date.getSeconds();
    const sec = (seconds < 10) ? `0${seconds}` : seconds;

    const newDateString = `${yr}-${mo}-${day}`;
    const newTimeString = `${hr}:${min}:${sec}`;

    return `${newDateString} ${newTimeString}`;
}
/**
 * @param {Object} response HTTP response from fetch()
 * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
 * @param {String} resource Name of the resource to fetch, e.g. 'posts'
 * @param {Object} params The REST request params, depending on the type
 * @returns {Object} REST response
 */
const convertHTTPResponseToREST = (response, type, resource, params) => {
    const { json } = response;

    switch (type) {
    case GET_LIST:
        return {
            data: json.map(x => x),
            total: Number(response.total),
        };
    case GET_ONE:
        return {
            data: json,
            total: Number(response.total),
        };
    case GET_MANY:
        return { data: json,
            total: Number(response.total) };
    case CREATE:
        return {
            data: json
        };
        case DELETE:
            return {data: params };
            case DELETE_MANY:
                return { data: params.ids }
    default:
        return { data: json,
            total: Number(response.total) };
    }
};
function titleCase(str) {
    const splitStr = str.toLowerCase().split(' ');
    for (let i = 0; i < splitStr.length; i++) {
        if (i === 0) { splitStr[i] = splitStr[i].charAt(0).toLowerCase() + splitStr[i].substring(1); } else { splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1); }
    }
    // Directly return the joined string
    return splitStr.join('');
}
/**
 * @param {string} type Request type, e.g GET_LIST
 * @param {string} resource Resource name, e.g. "posts"
 * @param {Object} payload Request parameters. Depends on the request type
 * @returns {Promise} the Promise for a REST response
 */
export default (type, resource, params) => {
    const { url, options } = convertRESTRequestToHTTP(type, resource, params);
    return fetchJson(url, options)
        .then(response => convertHTTPResponseToREST(response, type, resource, params));
};

