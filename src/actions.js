import _ from 'lodash';
export const FIRST_STEP = 'FIRST_STEP';
export const STEP_THREE = 'STEP_THREE';
export const FORTH_STEP = 'FORTH_STEP';
export const fillFirstStep = (initialValues) => {
    return{
        type: STEP_THREE,
        payload: { initialValues },
    }
};
export const fillThirdFormValues = (initialValues) => {
    return{
        type: STEP_THREE,
        payload: { initialValues },
    }
};
export const fillFourthFormValues = (initialValues) => {
    return{
        type: FORTH_STEP,
        payload: { initialValues },
    }
};
